package org.inra.ecoinfo.mga.configurator;

import org.inra.ecoinfo.mga.business.composite.INodeable;
import static org.inra.ecoinfo.mga.configurator.AbstractMgaIOConfigurator.DATASET_CONFIGURATION;
import static org.inra.ecoinfo.mga.configurator.AbstractMgaIOConfigurator.DATASET_CONFIGURATION_RIGHTS;
import static org.inra.ecoinfo.mga.configurator.AbstractMgaIOConfigurator.REFDATA_CONFIGURATION;
import static org.inra.ecoinfo.mga.configurator.AbstractMgaIOConfigurator.REFDATA_CONFIGURATION_RIGHTS;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.refdata.Refdata;
import org.inra.ecoinfo.refdata.theme.Theme;
import org.inra.ecoinfo.symphonie.refdata.projet.Projet;
import org.inra.ecoinfo.symphonie.refdata.site.SiteSymphonie;
import org.inra.ecoinfo.symphonie.refdata.typesite.TypeSite;

/**
 *
 * @author yahiaoui
 */
public class MgaIOConfigurator extends AbstractMgaIOConfigurator {

    private static final Integer[] NORMAL_ORDER_0_1_2_3_4 = new Integer[]{0, 1, 2, 3, 4};
    private static final Integer[] DATASET_ORDER_0 = new Integer[]{0};
    private static final Class<INodeable>[] ENTRY_INSTANCE_REF = new Class[]{
        Refdata.class
    };
    private static final Activities[] ACTIVITIES_ESTA
            = new Activities[]{
                Activities.edition,
                Activities.suppression,
                Activities.telechargement,
                Activities.administration};
    private static final Activities[] ACTIVITIES_A
            = new Activities[]{
                Activities.associate
            };
    private static final Class<INodeable>[] ENTRY_INSTANCE_TTSSDP = new Class[]{
        Theme.class,
        TypeSite.class,
        SiteSymphonie.class,
        DataType.class,
        Projet.class
    };
    private static final Activities[] ACTIVITIES_SAPDSE
            = new Activities[]{
                Activities.synthese,
                Activities.administration,
                Activities.publication,
                Activities.depot,
                Activities.suppression,
                Activities.extraction,};

    /**
     *
     */
    public MgaIOConfigurator() {
        super(new MgaDisplayerConfiguration(){
        });
    }

    /**
     *
     */
    @Override
    protected void initConfigurations() {

        /**
         * Configuration Zero
         */
        boolean includeAncestorDataset = true;

        WhichTree whichTreeDataSet = WhichTree.TREEDATASET;

        Class stickyLeafDatasetRights = Projet.class;

        boolean displayColumnNamesDataset = false;

        ConfigurationTest configDatasetRights
                = new ConfigurationTest(
                        DATASET_CONFIGURATION_RIGHTS,
                        Projet.class,
                        NORMAL_ORDER_0_1_2_3_4,
                        ENTRY_INSTANCE_TTSSDP,
                        ACTIVITIES_SAPDSE,
                        NORMAL_ORDER_0_1_2_3_4,
                        includeAncestorDataset,
                        whichTreeDataSet,
                        stickyLeafDatasetRights,
                        displayColumnNamesDataset);
        ConfigurationTest configDataset
                = new ConfigurationTest(
                        DATASET_CONFIGURATION,
                        Projet.class,
                        NORMAL_ORDER_0_1_2_3_4,
                        ENTRY_INSTANCE_TTSSDP,
                        ACTIVITIES_SAPDSE,
                        NORMAL_ORDER_0_1_2_3_4,
                        includeAncestorDataset,
                        whichTreeDataSet,
                        null,
                        displayColumnNamesDataset);

        /**
         * Configuration Zero
         */
        boolean includeAncestorRefdata = true;

        WhichTree whichTreeRefdata = WhichTree.TREEREFDATA;

        Class<Projet> stickyLeafRefdata = null;

        boolean displayColumnNamesRefdata = false;

        AbstractMgaIOConfiguration configRefdataRights
                = new ConfigurationTest(REFDATA_CONFIGURATION_RIGHTS,
                        Refdata.class,
                        DATASET_ORDER_0,
                        ENTRY_INSTANCE_REF,
                        ACTIVITIES_ESTA,
                        DATASET_ORDER_0,
                        includeAncestorRefdata,
                        whichTreeRefdata,
                        stickyLeafRefdata,
                        displayColumnNamesRefdata);

        AbstractMgaIOConfiguration configRefdata
                = new ConfigurationTest(REFDATA_CONFIGURATION,
                        Refdata.class,
                        DATASET_ORDER_0,
                        ENTRY_INSTANCE_REF,
                        ACTIVITIES_ESTA,
                        DATASET_ORDER_0,
                        includeAncestorRefdata,
                        whichTreeRefdata,
                        stickyLeafRefdata,
                        displayColumnNamesRefdata);

//        AbstractMgaIOConfiguration configExtraction
//                = new ConfigurationTest(SYNTHESIS_CONFIGURATION,
//                        DataType.class,
//                        NORMAL_ORDER_0_1_2_3,
//                        ENTRY_INSTANCE_TTSSDP,
//                        ACTIVITIES_SAPDSE,
//                        INVERSE_ORDER_3_0_2_1,
//                        includeAncestorDataset,
//                        whichTreeDataSet,
//                        null,
//                        displayColumnNamesDataset);

        /**
         * Configuration Associate // For RefData Insertion
         */
        AbstractMgaIOConfiguration configAssociate
                = new ConfigurationTest(ASSOCIATE_CONFIGURATION,
                        Projet.class,
                        NORMAL_ORDER_0_1_2_3_4,
                        ENTRY_INSTANCE_TTSSDP,
                        ACTIVITIES_A,
                        NORMAL_ORDER_0_1_2_3_4,
                        includeAncestorDataset,
                        whichTreeDataSet,
                        null,
                        displayColumnNamesDataset);
        /**
         * ------------------------------------------------------------------------------------
         * *
         */
        getConfigurations().computeIfAbsent(DATASET_CONFIGURATION_RIGHTS, k -> configDatasetRights);
        getConfigurations().computeIfAbsent(REFDATA_CONFIGURATION_RIGHTS, k -> configRefdataRights);
        getConfigurations().computeIfAbsent(REFDATA_CONFIGURATION, k -> configRefdata);
        getConfigurations().computeIfAbsent(DATASET_CONFIGURATION, k -> configDataset);
        getConfigurations().computeIfAbsent(ASSOCIATE_CONFIGURATION, k -> configAssociate);
//        getConfigurations().computeIfAbsent(SYNTHESIS_CONFIGURATION, k -> configExtraction);

        /**
         * ------------------------------------------------------------------------------------
         * *
         */
    }

    class MgaIOConfiguration extends AbstractMgaIOConfiguration {

        public MgaIOConfiguration(int codeConfig, Class<? extends INodeable> leafType, Integer[] entryOrder, Class<INodeable>[] entryTypes, Activities[] activities, Integer[] sortOrder, boolean includeAncestor, WhichTree whichTree, Class<? extends INodeable> stickyLeafType, boolean displayColumnNames) {
            super(codeConfig, leafType, entryOrder, entryTypes, activities, sortOrder, includeAncestor, whichTree, stickyLeafType, displayColumnNames);
        }

    }

    class ConfigurationTest extends AbstractMgaIOConfiguration {

        public ConfigurationTest(int codeConfig, Class<? extends INodeable> leafType, Integer[] entryOrder, Class<INodeable>[] entryTypes, Activities[] activities, Integer[] sortOrder, boolean includeAncestor, WhichTree whichTree, Class<? extends INodeable> stickyLeafType, boolean displayColumnNames) {
            super(codeConfig, leafType, entryOrder, entryTypes, activities, sortOrder, includeAncestor, whichTree, stickyLeafType, displayColumnNames);
        }

    }

}
