/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import org.inra.ecoinfo.utils.DateUtil;

/**
 *
 * @author ptcherniati
 */
public class Testeur {

    public static void main(String[] args) {
        long nano = LocalDateTime.now().toInstant(ZoneOffset.UTC).getEpochSecond();
        System.out.println(nano);

        nano = LocalDateTime.ofInstant(Instant.EPOCH, DateUtil.UTC_ZONEID).toInstant(ZoneOffset.UTC).getEpochSecond();
        System.out.println(nano);
        nano = LocalDateTime.now().getNano();
        System.out.println(nano);

        nano = LocalDateTime.ofInstant(Instant.EPOCH, DateUtil.UTC_ZONEID).getNano();
        System.out.println(nano);

    }
}
