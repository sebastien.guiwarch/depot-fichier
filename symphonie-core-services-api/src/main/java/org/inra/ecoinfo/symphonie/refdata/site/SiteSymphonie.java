package org.inra.ecoinfo.symphonie.refdata.site;

import java.util.Objects;
import static javax.persistence.CascadeType.*;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.symphonie.refdata.typesite.TypeSite;
import org.inra.ecoinfo.refdata.site.Site;

/**
 *
 * @author ptchernia
 */
@Entity(name = "SiteSymphonie")
@Table(name = SiteSymphonie.NAME_ENTITY_JPA,
        indexes = {
            @Index(name = "site_symphonie_typesite_idx", columnList = TypeSite.ID_JPA)
        })
@DiscriminatorValue(value="SiteSymphonieDisc")
@PrimaryKeyJoinColumn(name = "SiteSymphonie_ID")
public class SiteSymphonie extends Site {

    /**
     * The Constant ID_JPA.
     */
    static public final String ID_JPA           = "id";
    /**
     * The Constant TABLE_NAME.
     */
    public static final String TABLE_NAME       = "zones_etude_zet";
    /**
     * The Constant serialVersionUID.
     */
    private static final long  serialVersionUID = 1L;
    /**
     * The Constant NAME_ENTITY_JPA.
     */
    static public final String NAME_ENTITY_JPA  = "zones_etude_zet";
    /**
     * The Constant LOCALISATION.
     */
    public static final String LOCALISATION     = "localisation";
    /**
     * The type site.
     */
    @ManyToOne(cascade = {MERGE, PERSIST, REFRESH}, optional = false)
    @JoinColumn(name = TypeSite.ID_JPA, referencedColumnName = TypeSite.ID_JPA, nullable = false)
    private TypeSite           typeSite;
    /**
     *
     */
    public SiteSymphonie() {
        super();
    }

    /**
     *
     * @param nom
     */
    public SiteSymphonie(final String nom) {
        super(nom, null, null);
    }

    /**
     *
     * @param typeSite
     * @param parent
     * @param nom
     * @param description
     */
    public SiteSymphonie(final TypeSite typeSite, final SiteSymphonie parent, final String nom,
            final String description) {
        super(nom, description, parent);
        this.typeSite = typeSite;
        typeSite.addSite(this);
    }

    /**
     *
     * @return
     */
    @Override
    public Long getId() {
        return super.getId();
    }

    /**
     *
     * @return
     */
    public SiteSymphonie getRoot() {
        SiteSymphonie site = this;
        if (getParent() != null) {
            site = ((SiteSymphonie) getParent()).getRoot();
        }
        return site;
    }

    /**
     *
     * @return
     */
    public SiteSymphonie getRootSite() {
        SiteSymphonie root = this;
        while (root.getParent() != null) {
            root = (SiteSymphonie) root.getParent();
        }
        return root;
    }

    /**
     *
     * @return
     */
    public TypeSite getTypeSite() {
        return typeSite;
    }
    
    /**
     *
     * @param typeSite
     */
    public void setTypeSite(final TypeSite typeSite) {
        this.typeSite = typeSite;
    }

    @Override
    public String toString() {
        return "Site : " .concat(getCode());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.typeSite);       
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SiteSymphonie other = (SiteSymphonie) obj;
        return Objects.equals(this.typeSite, other.typeSite);
    }
    
    /**
     *
     * @param <T>
     * @return
     */

    @Override
    public <T extends INodeable> Class<T> getNodeableType() {
        return (Class<T>) SiteSymphonie.class;
    }

}
