package org.inra.ecoinfo.symphonie.refdata.typesite;

import java.util.LinkedList;
import java.util.List;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.symphonie.refdata.site.SiteSymphonie;

/**
 * The Class TypeSite.
 *
 * @author philippe
 * @author "Guillaume Enrico"
 */
@Entity(name = "TypeSitePEM")
@Table(name = TypeSite.NAME_ENTITY_JPA)
@PrimaryKeyJoinColumn(name = TypeSite.ID_JPA)
public class TypeSite extends Nodeable{

    /**
     * The Constant ID_JPA.
     */
    static public final String  ID_JPA          = "tze_id";
    /**
     * The Constant NAME_ENTITY_JPA.
     */
    static public final String  NAME_ENTITY_JPA = "types_zone_etude_tze";
    /**
     * The Constant STRING_EMPTY.
     */
    static private final String STRING_EMPTY    = "";
    /**
     * The definition.
     */
    @Column(nullable = true, name = "tze_definition", length = 150)
    private String              definition      = TypeSite.STRING_EMPTY;
    /**
     * The sites.
     */
    @OneToMany(mappedBy = "typeSite", cascade = ALL)
    @LazyToOne(LazyToOneOption.PROXY)
    private List<SiteSymphonie>  sites           = new LinkedList<>();

    /**
     * Instantiates a new type site.
     */
    public TypeSite() {
        super();
    }

    /**
     * Instantiates a new type site.
     *
     * @param code
     *            the code
     * @param nom
     *            the nom
     * @param definition
     *            the definition
     */
    public TypeSite(final String code, final String definition) {
        super();
        this.code = code;
        this.definition = definition;
    }

    /**
     * Adds the site.
     *
     * @param site
     *            the site
     */
    public void addSite(final SiteSymphonie site) {
        if (site == null) {
            return;
        }
        site.setTypeSite(this);
        if (!getSites().contains(site)) {
            getSites().add(site);
        }
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets the definition.
     *
     * @return the definition
     */
    public String getDefinition() {
        return definition == null ? TypeSite.STRING_EMPTY : definition;
    }

    /**
     * Gets the sites.
     *
     * @return the sites
     */
    public List<SiteSymphonie> getSites() {
        return sites;
    }

    /**
     * Sets the code.
     *
     * @param code
     *            the new code
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * Sets the definition.
     *
     * @param definition
     *            the new definition
     */
    public void setDefinition(final String definition) {
        this.definition = definition == null ? TypeSite.STRING_EMPTY : definition;
    }

    /**
     * Sets the sites.
     *
     * @param sites
     *            the new sites
     */
    public void setSites(final List<SiteSymphonie> sites) {
        this.sites = sites;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return code;
    }

    @Override
    public String getName() {
        return getCode();
    }

    @Override
    public <T extends INodeable> Class<T> getNodeableType() {
        return (Class<T>) TypeSite.class;
    }
}
