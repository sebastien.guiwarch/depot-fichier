package org.inra.ecoinfo.symphonie.refdata.projet;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.annotations.Type;
import org.inra.ecoinfo.mga.business.composite.Nodeable;

/**
 * Représente un theme scientifique.
 *
 * @author "Antoine Schellenberger"
 */
@Entity(name = "Projet")
@Table(name = Projet.TABLE_NAME)
public class Projet extends Nodeable implements Serializable {

        
    /**
     * The Constant serialVersionUID.
     */
    private static final long  serialVersionUID         = 1L;
    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA                     = "id";
//    public static final String ID_JPA                   = "pro_id";
    /**
     * The Constant TABLE_NAME.
     */
    public static final String TABLE_NAME               = "projet";
    /**
     * The Constant ENTITY_FIELD_DESCRIPTION.
     */
    public static final String ENTITY_FIELD_DESCRIPTION = "descriptionProjet";
    /**
     * The Constant NAME_ATTRIBUTS_NAME @link(String).
     */
    public static final String NAME_ATTRIBUTS_NAME = "name";
    /**
     * The description projet.
     */
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "definition")
    private String             descriptionProjet;
 
    /**
     * Instantiates a new projet.
     */
    public Projet() {
        super();
    }

    /**
     * Instantiates a new projet.
     *
     * @param nom
     *            the nom
     */
    public Projet(final String code) {
        setCode(code);
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    @Override
    public String getCode() {
        return code;
    }

    /**
     * Gets the description projet.
     *
     * @return the description projet
     */
    public String getDescriptionProjet() {
        return descriptionProjet;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    @Override
    public Long getId() {
        return super.getId();
    }

    /**
     * Sets the code.
     *
     * @param code
     *            the new code
     */
    @Override
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * Sets the description projet.
     *
     * @param descriptionProjet
     *            the new description projet
     */
    public void setDescriptionProjet(final String descriptionProjet) {
        this.descriptionProjet = descriptionProjet;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     */
    @Override
    public void setId(final Long id) {
        super.setId(id);
    }

    /**
     *
     * @return
     */
    @Override
    public String getName() {
        return getCode();
    }
    
     @Override
    public String toString() {
        return this.code;
    }
   
    /**
     *
     * @return
     */
    @Override
    public Class<? extends Nodeable> getNodeableType() {
        return Projet.class;
    }
}
