/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.symphonie.refdata.typesite;

import org.inra.ecoinfo.symphonie.refdata.typesite.TypeSite;
import java.util.Arrays;
import java.util.List;
import org.inra.ecoinfo.symphonie.refdata.site.SiteSymphonie;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author ptcherniati
 */
public class TypeSiteTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    public TypeSiteTest() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of addSite method, of class TypeSite.
     */
    @Test
    public void testAddSite() {
        SiteSymphonie site = new SiteSymphonie();
        TypeSite instance = new TypeSite();
        instance.addSite(site);
        Assert.assertEquals(site, instance.getSites().get(0));
    }

    /**
     * Test of getDefinition method, of class TypeSite.
     */
    @Test
    public void testGetDefinition() {
        TypeSite instance = new TypeSite();
        String expResult = "definition";
        instance.setDefinition(expResult);
        String result = instance.getDefinition();
        Assert.assertEquals(expResult, result);
        instance.setDefinition(null);
    }

    /**
     * Test of getId method, of class TypeSite.
     */
    @Test
    public void testGetId() {
        TypeSite instance = new TypeSite();
        Long expResult = 25L;
        instance.setId(expResult);
        Long result = instance.getId();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getNom method, of class TypeSite.
     */
    @Test
    public void testGetNom() {
        TypeSite instance = new TypeSite();
        String expResult = "Un nom accentué";
        instance.setCode(expResult);
        String result = instance.getName();
        Assert.assertEquals(expResult, result);
        Assert.assertEquals(expResult, instance.getCode());
    }

    /**
     * Test of getSites method, of class TypeSite.
     */
    @Test
    public void testGetSites() {
        TypeSite instance = new TypeSite();
        List<SiteSymphonie> expResult = Arrays.asList(new SiteSymphonie[] { new SiteSymphonie() });
        instance.setSites(expResult);
        List<SiteSymphonie> result = instance.getSites();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class TypeSite.
     */
    @Test
    public void testToString() {
        TypeSite instance = new TypeSite();
        String expResult = "nom";
        instance.setCode(expResult);
        String result = instance.toString();
        Assert.assertEquals(expResult, result);
    }

}
