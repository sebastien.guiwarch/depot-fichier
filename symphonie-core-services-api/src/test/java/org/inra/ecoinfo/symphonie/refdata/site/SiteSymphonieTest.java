/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.symphonie.refdata.site;


import org.inra.ecoinfo.symphonie.refdata.typesite.TypeSite;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author ptcherniati
 */
public class SiteSymphonieTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    SiteSymphonie s1;
    SiteSymphonie s2;
    SiteSymphonie s3;
    SiteSymphonie s4;
    SiteSymphonie s5;
    SiteSymphonie as1;

    TypeSite     ts1 = new TypeSite();

    TypeSite     ts2 = new TypeSite();

    /**
     *
     */
    public SiteSymphonieTest() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        s1 = new SiteSymphonie(ts1, as1, "nom1", "desciption1");
        as1 = new SiteSymphonie(ts1, as1, "nom1", "desciption1");
        s2 = new SiteSymphonie(ts1, as1, "nom2", "desciption1");
        s3 = new SiteSymphonie(ts1, s1, "nom1", "desciption1");
        s4 = new SiteSymphonie(ts1, null, "Nom4", "desciption1");
        s5 = new SiteSymphonie(ts1, s4, "Nom5", "desciption1");
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of equals method, of class SiteSymphonie.
     */
    @Test
    public void testEquals() {
        Assert.assertEquals(s1, s1);
        Assert.assertEquals(s1, as1);
        Assert.assertNotSame(s1, s2);
        Assert.assertNotSame(s1, s3);
    }

    /**
     * Test of getId method, of class SiteSymphonie.
     */
    @Test
    public void testGetId() {
        SiteSymphonie instance = new SiteSymphonie();
        Long expResult = 1L;
        instance.setId(expResult);
        Long result = instance.getId();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getRoot method, of class SiteSymphonie.
     */
    @Test
    public void testGetRoot() {
        SiteSymphonie result = s5.getRoot();
        Assert.assertEquals(s4, result);
    }

    /**
     * Test of getRootSite method, of class SiteSymphonie.
     */
    @Test
    public void testGetRootSite() {
        SiteSymphonie result = s5.getRoot();
        Assert.assertEquals(s4, result);
    }

    /**
     * Test of getTypeSite method, of class SiteSymphonie.
     */
    @Test
    public void testGetTypeSite() {
        new SiteSymphonie();
        Assert.assertEquals(ts1, s1.getTypeSite());
    }

    /**
     * Test of toString method, of class SiteSymphonie.
     */
    @Test
    public void testToString() {
        Assert.assertEquals("Site : Nom4/Nom5", s5.toString());
    }

}
