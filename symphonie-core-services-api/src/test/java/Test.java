
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;
import org.apache.commons.lang.ArrayUtils;
import org.apache.lucene.util.CollectionUtil;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ptcherniati
 */
public class Test {

    @org.junit.Test
    public void montest() throws Exception {
        List<cc> l = new LinkedList<>();
        final cc o = new cc("a", "b");
        l.add(o);
        l.add(new cc("aa", "bb"));
        l.stream()
                .map(cc -> new String[]{cc.getChamp1(), cc.getChamp2()})
                .map(a -> Arrays.asList(a))
                .findFirst()
                .ifPresent(o::setChamp1);
    }

    public static void maMethode(List<String> l) {
        System.out.println(l + " " + l.getClass());

    }

    class cc {

        public cc(String champ1, String champ2) {
            this.champ1 = champ1;
            this.champ2 = champ2;
        }

        public String getChamp1() {
            return champ1;
        }

        public void setChamp1(List l) {
            System.out.println(l + " " + l.getClass());
        }

        public void setChamp1(String champ1) {
            this.champ1 = champ1;
        }

        public String getChamp2() {
            return champ2;
        }

        public void setChamp2(String champ2) {
            this.champ2 = champ2;
        }
        String champ1;
        String champ2;
    }

}
