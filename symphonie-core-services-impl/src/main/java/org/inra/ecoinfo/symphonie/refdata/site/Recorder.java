/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.symphonie.refdata.site;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Properties;
import org.assertj.core.util.Strings;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.mga.middleware.IMgaRecorder;
import org.inra.ecoinfo.symphonie.refdata.typesite.ITypeSiteDAO;
import org.inra.ecoinfo.symphonie.refdata.typesite.TypeSite;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Class Recorder.
 * <p>
 *
 * @author "Guillaume Enrico"
 */
public class Recorder extends AbstractCSVMetadataRecorder<SiteSymphonie> {

    /**
     *
     */
    public static final String PROPERTY_MSG_MISSING_TYPE_SITE = "PROPERTY_MSG_MISSING_TYPE_SITE";

    /**
     *
     */
    public static final String PROPERTY_MSG_INVALID_TYPE_SITE = "PROPERTY_MSG_INVALID_TYPE_SITE";

    /**
     *
     */
    public static final String PROPERTY_MSG_MISSING_SITE = "PROPERTY_MSG_MISSING_SITE";

    /**
     *
     */
    public static final String PROPERTY_MSG_INVALID_SITE = "PROPERTY_MSG_INVALID_SITE";

    /**
     *
     */
    public static final String MONSOERE_REFDATA_BUNDLE_NAME = "org.inra.ecoinfo.symphonie.refdata.messages";

    /**
     * The Constant PROPERTY_CST_TYPE_ZONE.
     */
    private static final String PROPERTY_CST_TYPE_ZONE = "Type de zone";
    /**
     * The Constant PROPERTY_CST_NAME.
     */
    private static final String PROPERTY_CST_NAME = "nom";
    /**
     * The Constant PROPERTY_CST_DESCRIPTION.
     */
    private static final String PROPERTY_CST_DESCRIPTION = "description";
    /**
     * The Constant PROPERTY_CST_PARENT_PLACE.
     */
    private static final String PROPERTY_CST_PARENT_PLACE = "zone parente";
    /**
     * The site mon soere dao.
     */
    protected ISiteSymphonieDAO siteSymphonieDAO;

    /**
     *
     */
    protected IMgaRecorder mgaRecorder;

    /**
     * The type site dao.
     */
    protected ITypeSiteDAO typeSiteDAO;
    /**
     * The type sites possibles.
     */
    private String[] typeSitesPossibles;
    /**
     * The properties nom en.
     */
    private Properties propertiesNomFR;
    /**
     * The properties nom en.
     */
    private Properties propertiesNomEN;
    /**
     * The properties description en.
     */
    private Properties propertiesDescriptionEN;

    /**
     *
     * @param mgaRecorder
     */
    public void setMgaRecorder(IMgaRecorder mgaRecorder) {
        this.mgaRecorder = mgaRecorder;
    }

    /**
     * Creates the or update site.
     * <p>
     *
     * @param parent the parent
     * @param nom the nom
     * @param description the description
     * @param dbSite the db site
     * @param dbTypeSite the db type site
     * @throws PersistenceException the persistence exception
     */
    private void createOrUpdateSite(final SiteSymphonie parent, final String nom,
            final String description, final SiteSymphonie dbSite, final TypeSite dbTypeSite)
            throws BusinessException {
        if (dbSite == null) {
            createSite(parent, nom, description, dbTypeSite);
        } else {
            updateSite(parent, nom, description, dbSite, dbTypeSite);
        }
    }

    /**
     * Creates the site.
     * <p>
     *
     * @param parent the parent
     * @param nom the nom
     * @param description the description
     * @param dbTypeSite the db type site
     * @throws PersistenceException the persistence exception
     */
    private void createSite(final SiteSymphonie parent, final String nom, final String description,
            final TypeSite dbTypeSite) throws BusinessException {

        try {
            final SiteSymphonie site = new SiteSymphonie(dbTypeSite, parent, nom, description);
            siteSymphonieDAO.saveOrUpdate(site);
        } catch (PersistenceException ex) {
           throw new BusinessException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util
     * .CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        try {
            String[] values;
            values = parser.getLine();
            while (values != null) {
                final String code = values[1];
                final String nomPathParent = values[6];
                final SiteSymphonie parent = siteSymphonieDAO.getByPath(nomPathParent)
                        .map(s->(SiteSymphonie) s)
                        .orElse(null);
                mgaRecorder.remove(siteSymphonieDAO.getByNameAndParent(code, parent)
                        .orElseThrow(() -> new BusinessException("can't get site")));

                        
                values = parser.getLine();
            }
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<SiteSymphonie> getAllElements() throws BusinessException {
        final List<SiteSymphonie> sites = new LinkedList<>();
        siteSymphonieDAO.getAllSitesSymphonie().stream().forEach((siteSymphonie) -> {
            sites.add(siteSymphonie);
        });
        return sites;
    }

    /**
     * Gets the description site en.
     * <p>
     *
     * @param site the site
     * @return the description site en
     */
    private String getDescriptionSiteEN(final SiteSymphonie site) {
        String siteDescription = AbstractCSVMetadataRecorder.EMPTY_STRING;
        if (site != null && site.getDescription() != null) {
            siteDescription = propertiesDescriptionEN.getProperty(site.getDescription());
        }
        return siteDescription;
    }

    /**
     * Gets the description site fr.
     * <p>
     *
     * @param site the site
     * @return the description site fr
     */
    private String getDescriptionSiteFR(final SiteSymphonie site) {
        String siteDescription = AbstractCSVMetadataRecorder.EMPTY_STRING;
        if (site != null) {
            siteDescription = site.getDescription();
        }
        return siteDescription;
    }

    /**
     * Gets the names sites possibles.
     * <p>
     *
     * @return the names sites possibles
     * @throws PersistenceException the persistence exception
     */
    private String[] getNamesSitesPossibles() {
        final List<Site> sites = siteSymphonieDAO.getAll();
        final String[] namesSitesPossibles = new String[sites.size() + 1];
        namesSitesPossibles[0] = AbstractCSVMetadataRecorder.EMPTY_STRING;
        int index = 1;
        for (final Site site : sites) {
            namesSitesPossibles[index++] = site.getPath();
        }
        return namesSitesPossibles;
    }

    /**
     * Gets the names types sites possibles.
     * <p>
     *
     * @return the names types sites possibles
     * @throws PersistenceException the persistence exception
     */
    private String[] getNamesTypesSitesPossibles() throws PersistenceException {
        final List<TypeSite> typesSites = typeSiteDAO.getAll();
        final String[] namesTypesSitesPossibles = new String[typesSites.size()];
        int index = 0;
        for (final TypeSite typeSite : typesSites) {
            namesTypesSitesPossibles[index++] = typeSite.getName();
        }
        return namesTypesSitesPossibles;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(final SiteSymphonie site)
            throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(getNomTypeSite(site), typeSitesPossibles,
                        null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(site == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : site.getName(),
                        ColumnModelGridMetadata.NULL_VALUE_POSSIBLES, Recorder.PROPERTY_CST_NAME.concat("_key"),
                        true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(getNomSiteFR(site),
                        ColumnModelGridMetadata.NULL_VALUE_POSSIBLES, Recorder.PROPERTY_CST_NAME.concat("_fr"),
                        false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(getNomSiteEN(site),
                        ColumnModelGridMetadata.NULL_VALUE_POSSIBLES, Recorder.PROPERTY_CST_NAME.concat("_en"),
                        false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(getDescriptionSiteFR(site),
                        ColumnModelGridMetadata.NULL_VALUE_POSSIBLES,
                        Recorder.PROPERTY_CST_DESCRIPTION.concat("_fr"), false, true, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(getDescriptionSiteEN(site),
                        ColumnModelGridMetadata.NULL_VALUE_POSSIBLES,
                        Recorder.PROPERTY_CST_DESCRIPTION.concat("_en"), false, true, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(getSiteParent(site), getNamesSitesPossibles(),
                        Recorder.PROPERTY_CST_PARENT_PLACE, true, false, false));
        return lineModelGridMetadata;
    }

    /**
     * Gets the nom site en.
     * <p>
     *
     * @param site the site
     * @return the nom site en
     */
    private String getNomSiteEN(final SiteSymphonie site) {
        String siteNom = AbstractCSVMetadataRecorder.EMPTY_STRING;
        if (site != null) {
            siteNom = propertiesNomEN.getProperty(site.getName());
        }
        return siteNom;
    }

    /**
     * Gets the nom site fr.
     * <p>
     *
     * @param site the site
     * @return the nom site fr
     */
    private String getNomSiteFR(final SiteSymphonie site) {
        String siteNom = AbstractCSVMetadataRecorder.EMPTY_STRING;
        if (site != null) {
            siteNom = propertiesNomFR.getProperty(site.getName());
        }
        return siteNom;
    }

    /**
     * Gets the nom type site.
     * <p>
     *
     * @param site the site
     * @return the nom type site
     */
    private String getNomTypeSite(final SiteSymphonie site) {
        String typeSiteNom = AbstractCSVMetadataRecorder.EMPTY_STRING;
        if (site != null) {
            typeSiteNom = site.getTypeSite().getName();
        }
        return typeSiteNom;
    }

    /**
     * Gets the properties description en.
     * <p>
     *
     * @return the properties description en
     */
    public Properties getPropertiesDescriptionEN() {
        return propertiesDescriptionEN;
    }

    /**
     * Gets the properties nom en.
     * <p>
     *
     * @return the properties nom en
     */
    public Properties getPropertiesNomEN() {
        return propertiesNomEN;
    }

    /**
     * Gets the site mon soere dao.
     * <p>
     *
     * @return the site mon soere dao
     */
    public ISiteSymphonieDAO getSiteSymphonieDAO() {
        return siteSymphonieDAO;
    }

    /**
     * Gets the site parent.
     * <p>
     *
     * @param site the site
     * @return the site parent
     */
    private String getSiteParent(final SiteSymphonie site) {
        String siteParent = AbstractCSVMetadataRecorder.EMPTY_STRING;
        if (site != null && site.getParent() != null) {
            siteParent = site.getParent().getPath();
        }
        return siteParent;
    }

    /**
     * Gets the type site dao.
     * <p>
     *
     * @return the type site dao
     */
    public ITypeSiteDAO getTypeSiteDAO() {
        return typeSiteDAO;
    }

    /**
     * Gets the type sites possibles.
     * <p>
     *
     * @return the type sites possibles
     */
    public String[] getTypeSitesPossibles() {
        return typeSitesPossibles == null ? null : Arrays.copyOf(typeSitesPossibles,
                typeSitesPossibles.length);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#initModelGridMetadata()
     */
    @Override
    protected ModelGridMetadata<SiteSymphonie> initModelGridMetadata() {
        try {
            typeSitesPossibles = getNamesTypesSitesPossibles();
            propertiesNomFR = localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteSymphonie.class), Nodeable.ENTITE_COLUMN_NAME, Locale.FRANCE);
            propertiesNomEN = localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteSymphonie.class), Nodeable.ENTITE_COLUMN_NAME, Locale.ENGLISH);
            propertiesDescriptionEN = localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteSymphonie.class),
                    "description", Locale.ENGLISH);
        } catch (final PersistenceException e) {
            LOGGER.info("can't find properties", e);
        }
        return super.initModelGridMetadata();
    }

    /**
     * Persist site.
     * <p>
     *
     * @param errorsReport the errors report
     * @param typeSiteCode the type site code
     * @param nomPathParent the nom path parent
     * @param nom the nom
     * @param description the description
     * @param lineNumber the line number
     * @throws PersistenceException the persistence exception
     */
    private void persistSite(final ErrorsReport errorsReport, final String typeSiteCode,
            final String nomPathParent, final String nom, final String description,
            final int lineNumber) throws BusinessException {
        Optional<TypeSite> dbTypeSiteOpt = typeSiteDAO.getTypeSiteByCode(typeSiteCode);
        if (!dbTypeSiteOpt.isPresent()) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(MONSOERE_REFDATA_BUNDLE_NAME,PROPERTY_MSG_INVALID_TYPE_SITE), lineNumber, 1, typeSiteCode));
            return;
        }
        SiteSymphonie parent = siteSymphonieDAO.getByPath(nomPathParent)
                .map(s->(SiteSymphonie) s)
                .orElse(null);
        if (parent == null && !Strings.isNullOrEmpty(nomPathParent)) {
            errorsReport
                    .addErrorMessage(String.format(localizationManager.getMessage(MONSOERE_REFDATA_BUNDLE_NAME,PROPERTY_MSG_INVALID_SITE), lineNumber, 8, nomPathParent));
            return;
        }
        final String path = parent == null ? nom : parent.getPath()
                .concat(PatternConfigurator.ANCESTOR_SEPARATOR)
                .concat(nom);
        final SiteSymphonie dbSite = siteSymphonieDAO.getByPath(path)
                .map(s->(SiteSymphonie) s)
                .orElse(null);
        if (!errorsReport.hasErrors()) {
            createOrUpdateSite(parent, nom, description, dbSite, dbTypeSiteOpt.get());
        }
    }

    /**
     * Process record.
     * <p>
     *
     * @param parser the parser
     * @param file the file
     * @param encoding the encoding
     * @throws BusinessException the business exception
     */
    @Override
    public void processRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            // On parcourt chaque ligne du fichier
            String[] values;
            int lineNumber = 0;
            Boolean loopEnd = false;
            while (!loopEnd) {
                loopEnd = !loopEnd;
                values = parser.getLine();
                if (values != null) {
                    final TokenizerValues tokenizerValues = new TokenizerValues(values, Nodeable.getLocalisationEntite(SiteSymphonie.class));
                    lineNumber++;
                    final String typeSiteCode = tokenizerValues.nextToken();
                    final String nom = tokenizerValues.nextToken();
                    final String description = tokenizerValues.nextToken();
                    final String nomPathParent = tokenizerValues.nextToken();
                    persistSite(errorsReport, typeSiteCode, nomPathParent, nom, description,
                            lineNumber);
                    loopEnd = false;
                }
            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Sets the properties description en.
     * <p>
     *
     * @param propertiesDescriptionEN the new properties description en
     */
    public void setPropertiesDescriptionEN(final Properties propertiesDescriptionEN) {
        this.propertiesDescriptionEN = propertiesDescriptionEN;
    }

    /**
     * Sets the properties nom en.
     * <p>
     *
     * @param propertiesNomEN the new properties nom en
     */
    public void setPropertiesNomEN(final Properties propertiesNomEN) {
        this.propertiesNomEN = propertiesNomEN;
    }

    /**
     * Sets the site mon soere dao.
     * <p>
     *
     * @param siteDAO the new site mon soere dao
     */
    public void setSiteSymphonieDAO(final ISiteSymphonieDAO siteDAO) {
        siteSymphonieDAO = siteDAO;
    }

    /**
     * Sets the type site dao.
     * <p>
     *
     * @param typeSiteDAO the new type site dao
     */
    public void setTypeSiteDAO(final ITypeSiteDAO typeSiteDAO) {
        this.typeSiteDAO = typeSiteDAO;
    }

    /**
     * Sets the type sites possibles.
     * <p>
     *
     * @param newTypeSitesPossibles
     */
    public void setTypeSitesPossibles(final String[] newTypeSitesPossibles) {
        if (newTypeSitesPossibles == null) {
            this.typeSitesPossibles = null;
        } else {
            this.typeSitesPossibles = Arrays.copyOf(newTypeSitesPossibles,
                    newTypeSitesPossibles.length);
        }
    }

    /**
     * Update site.
     * <p>
     *
     * @param parent the parent
     * @param nom the nom
     * @param description the description
     * @param dbSite the db site
     * @param dbTypeSite the db type site
     * @throws PersistenceException the persistence exception
     */
    private void updateSite(final SiteSymphonie parent, final String nom, final String description,
            SiteSymphonie dbSite, TypeSite dbTypeSite) throws BusinessException {
        try {
            dbSite.setTypeSite(dbTypeSite);
            dbSite.setParent(parent);
            dbSite.setName(nom);
            dbSite.setDescription(description);
            dbTypeSite.addSite(dbSite);
            siteSymphonieDAO.saveOrUpdate(dbSite);
            siteSymphonieDAO.flush();
            typeSiteDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException(ex);
        }
    }
}
