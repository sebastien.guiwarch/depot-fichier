/**
 * .OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:53:17
 */
package org.inra.ecoinfo.symphonie.refdata.projet;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.symphonie.refdata.projet.Projet;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Interface IProjetDAO.
 *
 * @author "Antoine Schellenberger"
 */
public interface IProjetDAO extends IDAO<Projet> {

    /**
     * Gets the all.
     *
     * @return the all
     * @throws PersistenceException
     *             the persistence exception
     */
    List<Projet> getAll() throws PersistenceException;

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.symphonie.refdata.projet.IProjetDAO#getByCode(java.lang.String)
     */

    /**
     *
     * @param code
     * @return
     */

    Optional<Projet> getByCode(final String code);
}
