/**
 */
package org.inra.ecoinfo.symphonie.refdata.site;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.mga.business.composite.Nodeable_;
import org.inra.ecoinfo.symphonie.refdata.site.SiteSymphonie;
import org.inra.ecoinfo.symphonie.refdata.site.SiteSymphonie_;
import org.inra.ecoinfo.refdata.site.JPASiteDAO;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.site.Site_;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Class JPASiteSymphonieDAO.
 *
 * @author philippe
 * @author "Guillaume Enrico"
 */
public class JPASiteSymphonieDAO extends JPASiteDAO implements ISiteSymphonieDAO {

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.symphonie.refdata.site.ISiteSymphonieDAO#getAllSitesSymphonie()
     */
    /**
     *
     * @return
     */
    @Override
    public List<SiteSymphonie> getAllSitesSymphonie() {
        return getAll().stream()
                .map(t -> (SiteSymphonie) t)
                .collect(Collectors.toList());
    }

    /*
     * *
     * 
     * @see org.inra.ore.forets.plugins.forets1.data.dao.ISiteDAO#getSiteByName(java .lang.String)
     */
 /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.site.JPASiteDAO#getByCode(java.lang.String)
     */
    @Override
    public Optional<Site> getByCodeAndParent(final String code, Site parent) {
        CriteriaQuery<SiteSymphonie> query = builder.createQuery(SiteSymphonie.class);
        Root<SiteSymphonie> sit = query.from(SiteSymphonie.class);
        query
                .select(sit)
                .where(
                        builder.equal(sit.get(Nodeable_.code), code),
                        builder.equal(sit.get(SiteSymphonie_.parent), parent));
        return getOptional(query).map(s -> (Site) s);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.inra.ecoinfo.symphonie.refdata.site.ISiteSymphonieDAO#getByNameAndParent(java.lang.String,
     * org.inra.ecoinfo.symphonie.refdata.site.SiteSymphonie)
     */
    /**
     *
     * @param name
     * @param parent
     * @return
     */
    @Override
    public Optional<SiteSymphonie> getByNameAndParent(final String name, final SiteSymphonie parent) {
        CriteriaQuery<SiteSymphonie> query = builder.createQuery(SiteSymphonie.class);
        Root<SiteSymphonie> sit = query.from(SiteSymphonie.class);
        if (parent == null) {
            query.where(
                    builder.equal(sit.get(Site_.name), name),
                    builder.isNull(sit.get(Site_.parent))
            );
        } else {
            query.where(
                    builder.equal(sit.get(Site_.name), name),
                    builder.equal(sit.get(Site_.parent), parent)
            );
        }
        query.select(sit);
        return getOptional(query);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.symphonie.refdata.site.ISiteSymphonieDAO#getListSite()
     */
    /**
     *
     * @return @throws PersistenceException
     */
    @Override
    public Map<String, SiteSymphonie> getListSite() throws PersistenceException {
        return getAll().stream()
                .collect(Collectors.toMap(s -> s.getPath(), s -> (SiteSymphonie) s));
    }
}
