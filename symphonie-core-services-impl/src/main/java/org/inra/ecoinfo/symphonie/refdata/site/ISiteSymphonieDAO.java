/**
 * OREIForets project - see LICENCE.txt for use created: 5 mai 2009 11:53:17
 */
package org.inra.ecoinfo.symphonie.refdata.site;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.inra.ecoinfo.symphonie.refdata.site.SiteSymphonie;
import org.inra.ecoinfo.refdata.site.ISiteDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Interface ISiteSymphonieDAO.
 *
 * @author philippe
 * @author "Guillaume Enrico"
 */
public interface ISiteSymphonieDAO extends ISiteDAO {

    /**
     * Gets the all sites mon soere.
     *
     * @return the all sites mon soere
     */
    List<SiteSymphonie> getAllSitesSymphonie();

    /**
     * Gets the by name and parent.
     *
     * @param code
     *            the code
     * @param parent
     *            the parent
     * @return the by name and parent
     */
    Optional<SiteSymphonie> getByNameAndParent(String code, SiteSymphonie parent);

    /**
     * Gets the list site.
     *
     * @return the list site
     * @throws PersistenceException
     *             the persistence exception
     */
    Map<String, SiteSymphonie> getListSite() throws PersistenceException;
}
