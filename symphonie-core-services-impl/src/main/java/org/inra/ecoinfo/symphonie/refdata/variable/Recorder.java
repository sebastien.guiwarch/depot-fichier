/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.symphonie.refdata.variable;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.stream.Collectors;
import org.inra.ecoinfo.mga.business.IMgaServiceBuilder;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.symphonie.refdata.variable.VariableSymphonie;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Class Recorder.
 *
 * @author "Philippe"
 * @author "Guillaume Enrico"
 */
public class Recorder extends AbstractCSVMetadataRecorder<VariableSymphonie> {

    /**
     * The variable dao.
     */
    protected IVariableDAO      variableDAO;
    /**
     * The names properties.
     */
    private Properties          namesFRProperties;
    /**
     * The names properties.
     */
    private Properties          namesENProperties;
    /**
     * The definitions properties.
     */
    private Properties          definitionsProperties;
    
    /*
     * (non-Javadoc)
     * 
     * @see
     * org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util
     * .CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final String code = values[0];
                variableDAO.remove(variableDAO.getByCode(code)
                        .orElseThrow(() -> new BusinessException("can't get variable")));

                values = parser.getLine();
            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<VariableSymphonie> getAllElements() throws BusinessException {
        return variableDAO.getAll()
                .stream()
                .map(v-> (VariableSymphonie)v)
                .collect(Collectors.toList());
    }

    /**
     * Gets the definitions properties.
     *
     * @return the definitions properties
     */
    public Properties getDefinitionsProperties() {
        return definitionsProperties;
    }

    /**
     * Gets the names properties.
     *
     * @return the names properties
     */
    public Properties getNamesENProperties() {
        return namesENProperties;
    }
      

    /**
     * Gets the names properties.
     *
     * @return the names properties
     */
    public Properties getNamesFRProperties() {
        return namesFRProperties;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(final VariableSymphonie variable) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        variable == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : variable
                                .getName(), ColumnModelGridMetadata.NULL_VALUE_POSSIBLES, null,
                        true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        variable == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : namesFRProperties.getProperty(variable.getName()),
                        ColumnModelGridMetadata.NULL_VALUE_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        variable == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : namesENProperties.getProperty(variable.getName()),
                        ColumnModelGridMetadata.NULL_VALUE_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        variable == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : variable
                                .getDefinition(), ColumnModelGridMetadata.NULL_VALUE_POSSIBLES,
                        null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        variable == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : definitionsProperties.getProperty(variable.getDefinition()),
                        ColumnModelGridMetadata.NULL_VALUE_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(variable == null ? "false" : variable
                        .getIsQualitative().toString(), new String[] { "true", "false" }, null,
                        false, false, false));
        return lineModelGridMetadata;
    }

    /**
     * Gets the variable dao.
     *
     * @return the variable dao
     */
    public IVariableDAO getVariableDAO() {
        return variableDAO;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#initModelGridMetadata()
     */
    @Override
    protected ModelGridMetadata<VariableSymphonie> initModelGridMetadata() {
        namesFRProperties = localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableSymphonie.class), Nodeable.ENTITE_COLUMN_NAME, Locale.FRANCE);
        namesENProperties = localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableSymphonie.class), Nodeable.ENTITE_COLUMN_NAME, Locale.ENGLISH);
        definitionsProperties = localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableSymphonie.class), "definition",
                Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    /**
     * Persist variable.
     *
     * @param errorsReport
     *            the errors report
     * @param nom
     *            the nom
     * @param code
     *            the code
     * @param definition
     *            the definition
     * @param isQualitative
     *            the is qualitative
     * @param lineNumber
     *            the line number
     * @throws PersistenceException
     *             the persistence exception
     */
    private Nodeable persistVariable(final String code,
            final String definition, final Boolean isQualitative) throws PersistenceException {
        Variable dbVariable = variableDAO.getByCode(code).orElse(null);
        if (dbVariable == null) {
            dbVariable = new VariableSymphonie(code, isQualitative);                           
        } else {
            dbVariable.setIsQualitative(isQualitative);
        }
        dbVariable.setDefinition(definition);

        variableDAO.saveOrUpdate(dbVariable);
        return dbVariable ;
    }

    /**
     * Process record.
     *
     * @param parser
     *            the parser
     * @param file
     *            the file
     * @param encoding
     *            the encoding
     * @throws BusinessException
     *             the business exception
     */
    @Override
    public void processRecord(final CSVParser parser, final File file, final String encoding)
            throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            // On parcourt chaque ligne du fichier
            String[] values = parser.getLine();
            int lineNumber = 0;
            
           
            while (values != null) {
                lineNumber++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, VariableSymphonie.NAME_ENTITY_JPA);
                final String code = tokenizerValues.nextToken();
                final String definition = tokenizerValues.nextToken();
                final String isQualitativeValue = tokenizerValues.nextToken();
                Boolean isQualitative = false;
                try {
                    isQualitative = Boolean.parseBoolean(isQualitativeValue);
                } catch (final Exception e) {
                    LOGGER.info("can't parse isQualitative", e);
                }
                               
                persistVariable(code, definition, isQualitative);
                
                values = parser.getLine();
            }
            
    
            
            
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }          
                       
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Sets the definitions properties.
     *
     * @param definitionsProperties
     *            the new definitions properties
     */
    public void setDefinitionsProperties(final Properties definitionsProperties) {
        this.definitionsProperties = definitionsProperties;
    }

    /**
     * Sets the names properties.
     *
     * @param namesProperties
     *            the new names properties
     */
    public void setNamesFRProperties(final Properties namesProperties) {
        this.namesFRProperties = namesProperties;
    }

    /**
     * Sets the names properties.
     *
     * @param namesProperties
     *            the new names properties
     */
    public void setNamesENProperties(final Properties namesProperties) {
        this.namesENProperties = namesProperties;
    }

    /**
     * Sets the variable dao.
     *
     * @param variableDAO
     *            the new variable dao
     */
    public void setVariableDAO(final IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    /**
     *
     * @return
     */
    public IMgaServiceBuilder getMgaServiceBuilder() {
        return mgaServiceBuilder;
    }

    /**
     *
     * @param mgaServiceBuilder
     */
    @Override
    public void setMgaServiceBuilder(IMgaServiceBuilder mgaServiceBuilder) {
        this.mgaServiceBuilder = mgaServiceBuilder;
    }

    
}
