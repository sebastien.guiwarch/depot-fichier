/**
 * OREIForets project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.symphonie.refdata.typesite;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.mga.business.composite.Nodeable_;
import org.inra.ecoinfo.symphonie.refdata.typesite.TypeSite;
import org.inra.ecoinfo.symphonie.refdata.typesite.TypeSite_;

/**
 * The Class JPATypeSiteDAO.
 *
 * @author philippe
 * @author "Guillaume Enrico"
 */
public class JPATypeSiteDAO extends AbstractJPADAO<TypeSite> implements ITypeSiteDAO {

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.symphonie.refdata.typesite.ITypeSiteDAO#getAll()
     */
    @Override
    public List<TypeSite> getAll() {
        return getAll(TypeSite.class);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.inra.ecoinfo.symphonie.refdata.typesite.ITypeSiteDAO#getTypeSiteByCode(java.lang.String)
     */
    @Override
    public Optional<TypeSite> getTypeSiteByCode(final String code) {
        CriteriaQuery<TypeSite> query = builder.createQuery(TypeSite.class);
        Root<TypeSite> ts = query.from(TypeSite.class);
        query
                .select(ts)
                .where(builder.equal(ts.get(Nodeable_.code), code));
        return getOptional(query);
    }

}
