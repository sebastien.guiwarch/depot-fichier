package org.inra.ecoinfo.symphonie;

import java.io.IOException;
import java.sql.SQLException;
import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.DataSetException;
import org.inra.ecoinfo.TransactionalTestFixtureExecutionListener;
import org.inra.ecoinfo.dataset.IDatasetManager;
import org.inra.ecoinfo.dataset.versioning.IDatasetDAO;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.symphonie.refdata.projet.IProjetDAO;
import org.inra.ecoinfo.symphonie.refdata.projetsitethemedatatype.IProjetSiteThemeDatatypeDAO;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.refdata.IMetadataManager;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.springframework.test.context.TestContext;
import org.springframework.transaction.annotation.Transactional;
import org.inra.ecoinfo.symphonie.refdata.site.ISiteSymphonieDAO;

/**
 *
 * @author tcherniatinsky
 */
public class SymphonieTransactionalTestFixtureExecutionListener extends TransactionalTestFixtureExecutionListener {

    private static String testName = "";

    /**
     *
     */
    public static IMetadataManager metadataManager;

    /**
     *
     */
    public static IDatasetManager datasetManager;

    /**
     *
     */
    public static IProjetSiteThemeDatatypeDAO projetSiteThemeDatatypeDAO;

    /**
     *
     */
    public static IDatasetDAO datasetDAO;

    /**
     *
     */
    public static IVersionFileDAO versionFileDAO;

    /**
     *
     */
    public static IProjetDAO projetDAO;

    /**
     *
     */
    public static ISiteSymphonieDAO siteSymphonieDAO;

    /**
     *
     */
    public static IVariableDAO variableDAO;

    /**
     *
     */
    public static ILocalizationManager localizationManager;

    /**
     *
     */
    public static INotificationsManager notificationsManager;

    /**
     * After super class.
     *
     * @param testContext
     * @link(TestContext) the test context
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws DataSetException the data set exception
     * @throws DatabaseUnitException the database unit exception
     * @throws SQLException the sQL exception
     * @throws Exception the exception
     * @link(TestContext) the test context
     */
    @Transactional(rollbackFor = Exception.class)
    void afterSuperClass(final TestContext testContext) throws IOException, DataSetException, DatabaseUnitException, SQLException, Exception {
        super.afterTestClass(testContext);
    }

    /**
     * Before test class.
     *
     * @param testContext
     * @link(TestContext) the test context
     * @throws Exception the exception @see
     * org.springframework.test.context.support.AbstractTestExecutionListener
     * #beforeTestClass(org.springframework.test.context.TestContext)
     */
    @Override
    public void beforeTestClass(final TestContext testContext) throws Exception {
        super.beforeTestClass(testContext);
        SymphonieTransactionalTestFixtureExecutionListener.metadataManager = (IMetadataManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("metadataManager");
        SymphonieTransactionalTestFixtureExecutionListener.datasetManager = (IDatasetManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("datasetManager");
        SymphonieTransactionalTestFixtureExecutionListener.projetSiteThemeDatatypeDAO = (IProjetSiteThemeDatatypeDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("projetSiteThemeDatatypeDAO");
        SymphonieTransactionalTestFixtureExecutionListener.datasetDAO = (IDatasetDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("datasetDAO");
        SymphonieTransactionalTestFixtureExecutionListener.versionFileDAO = (IVersionFileDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("versionFileDAO");
        SymphonieTransactionalTestFixtureExecutionListener.projetDAO = (IProjetDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("projetDAO");
        SymphonieTransactionalTestFixtureExecutionListener.siteSymphonieDAO = (ISiteSymphonieDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("siteSymphonieDAO");
        SymphonieTransactionalTestFixtureExecutionListener.variableDAO = (IVariableDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("variableDAO");
        SymphonieTransactionalTestFixtureExecutionListener.localizationManager = (ILocalizationManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("localizationManager");
        SymphonieTransactionalTestFixtureExecutionListener.notificationsManager = (INotificationsManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("notificationsManager");
        
        if (testName.isEmpty()) {
            testName = "../target/concordion/" + testContext.getTestClass().getName().replaceAll(".*\\.(.*?)Fixture", "$1");
        }
        System.setProperty("concordion.output.dir", testName);
    }

    /**
     * Clean tables.
     *
     * @throws SQLException the sQL exception
     */
    @Override
    protected void cleanTables() throws SQLException {
        getDatasourceConnexion()
                .getConnection()
                .prepareStatement(
                        "delete from group_utilisateur cascade;")
                .execute();
        getDatasourceConnexion()
                .getConnection()
                .prepareStatement(
                        "delete from utilisateur cascade;")
                .execute();
        getDatasourceConnexion()
                .getConnection()
                .prepareStatement(
                        "delete from groupe where group_name!='public';")
                .execute();
    }
}
