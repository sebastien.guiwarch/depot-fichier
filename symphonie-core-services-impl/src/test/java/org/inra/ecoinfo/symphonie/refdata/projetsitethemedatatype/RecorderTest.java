/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.symphonie.refdata.projetsitethemedatatype;

import com.Ostermiller.util.CSVParser;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.inra.ecoinfo.localization.impl.SpringLocalizationManager;
import org.inra.ecoinfo.mga.business.composite.ICompositeTreeNode;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.caching.ITreeApplicationCacheManager;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfiguration;
import org.inra.ecoinfo.mga.configurator.MgaIOConfigurator;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.symphonie.refdata.site.SiteSymphonie;
import org.inra.ecoinfo.symphonie.refdata.variable.VariableSymphonie;
import org.inra.ecoinfo.symphonie.test.utils.MockUtils;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.theme.Theme;
import org.inra.ecoinfo.symphonie.refdata.projet.Projet;
import org.inra.ecoinfo.symphonie.refdata.typesite.TypeSite;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author ptcherniati
 */
public class RecorderTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    MockUtils m = MockUtils.getInstance();
    Recorder instance;
    String encoding = "UTF-8";

    @Mock
    Properties propertiesNomEN;

    @Mock
    Properties propertiesDefinitionEN;
    @Mock
    List<INode> nodes;
    @Mock
    INode node;
    List<INodeable> nodeables = new LinkedList();
    @Mock
    IMgaIOConfiguration configuration;
    @Mock
    ITreeApplicationCacheManager applicationCacheManager;

    /**
     *
     */
    public RecorderTest() {
    }

    /**
     *
     * @throws PersistenceException
     */
    @Before
    public void setUp() throws PersistenceException {
        m = MockUtils.getInstance();
        instance = new Recorder();
        MockitoAnnotations.initMocks(this);
        instance.setMgaServiceBuilder(m.mgaServiceBuilder);
        instance.setProjetDAO(m.projetDAO);
        instance.setProjetSiteThemeDatatypeDAO(m.projetSiteThemeDatatypeDAO);
        instance.setSiteDAO(m.siteDAO);
        instance.setThemeDAO(m.themeDAO);
        instance.setDatatypeDAO(m.datatypeDAO);
        instance.setPolicyManager(m.policyManager);
        instance.setTreeApplicationCacheManager(applicationCacheManager);
        ((SpringLocalizationManager) MockUtils.localizationManager)
                .setLocalizationDAO(m.localizationDAO);
        instance.setLocalizationManager(MockUtils.localizationManager);
        when(m.localizationDAO.newProperties(Projet.TABLE_NAME, "nom", Locale.ENGLISH))
                .thenReturn(propertiesNomEN);
        when(
                m.localizationDAO.newProperties(Projet.TABLE_NAME, "definition", Locale.ENGLISH))
                .thenReturn(propertiesDefinitionEN);
        when(propertiesNomEN.getProperty("projet")).thenReturn("projeten");
        when(propertiesDefinitionEN.getProperty("Définition")).thenReturn("definitionen");
        when(m.recorder.getNodeables()).thenReturn(nodeables.stream());
        when(m.mgaServiceBuilder.getMgaIOConfigurator()).thenReturn(m.mgaIOConfigurator);
        when(m.mgaIOConfigurator.getConfiguration(2)).thenReturn(Optional.of(configuration));

    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of deleteRecord method, of class Recorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    @Ignore
    public void testDeleteRecord() throws Exception {
        List<INode> projetSiteThemeDatatypeNodes = Arrays.asList(new INode[]{m.projetSiteThemeDatatypeNode, m.projetSiteThemeNode, m.projetSiteNode, m.projetNode});
        String text = "Projet manche;oir/p1;Données biologiques;Piégeage en montée";
        when(
                m.projetSiteThemeDatatypeDAO.getByProjetPathSiteThemeCodeAndDatatypeCode(
                        "projet_manche", "oir/p1", "donnees_biologiques", "piegeage_en_montee"))
                .thenReturn(projetSiteThemeDatatypeNodes);
        CSVParser parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        File file = null;
        when(configuration.getWhichTree()).thenReturn(WhichTree.TREEDATASET);
        doNothing().when(m.recorder).removeStickyLeaves(m.projetSiteThemeDatatypeNode, WhichTree.TREEDATASET);
        // nominal case
        instance.deleteRecord(parser, file, encoding);
        verify(m.projetSiteThemeDatatypeDAO).remove(m.projetSiteThemeDatatypeNode);
        verify(m.projetSiteThemeDatatypeDAO).remove(m.projetSiteThemeNode);
        verify(m.projetSiteThemeDatatypeDAO).remove(m.projetSiteNode);
        verify(m.projetSiteThemeDatatypeDAO).remove(m.projetNode);
        verify(m.recorder).removeStickyLeaves(m.projetSiteThemeDatatypeNode, WhichTree.TREEDATASET);
        // bad type site code
        text = "Projet manche bad;oir/p1;Données biologiques;Piégeage en montée";
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        when(
                m.projetSiteThemeDatatypeDAO.getByProjetPathSiteThemeCodeAndDatatypeCode(
                        "projet_manche_bad", "oir/p1", "donnees_biologiques", "piegeage_en_montee"))
                .thenThrow(new PersistenceException("error"));
        BusinessException error = null;
        try {
            instance.deleteRecord(parser, file, encoding);
        } catch (BusinessException e) {
            error = e;
        }
        Assert.assertNotNull("une exception doit être lancée", error);
        Assert.assertEquals("error", error.getMessage());
        error = null;
        // peristence exception
        text = "Projet manche;oir/p1;Données biologiques;Piégeage en montée";
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        doThrow(new PersistenceException("error2")).when(m.projetSiteThemeDatatypeDAO)
                .remove(m.projetNode);
        try {
            instance.deleteRecord(parser, file, encoding);
        } catch (BusinessException e) {
            error = e;
        }
        Assert.assertNotNull("une exception doit être lancée", error);
        Assert.assertEquals("error2", error.getMessage());
    }

    /**
     * Test of getAllElements method, of class Recorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetAllElements() throws Exception {
        List<INode> nodes = Arrays.asList(new INode[]{node});
        doReturn(nodes.stream()).when(m.mgaServiceBuilder).loadNodes(configuration, false, false);
        List<INode> result = instance.getAllElements();
        verify(m.mgaServiceBuilder).loadNodes(configuration, false, false);
        Assert.assertEquals(nodes, result);
    }

    /**
     * Test of getDatatypeDAO method, of class Recorder.
     */
    @Test
    public void testGetDatatypeDAO() {
        instance = new Recorder();
        instance.setDatatypeDAO(m.datatypeDAO);
        Assert.assertEquals(m.datatypeDAO, instance.getDatatypeDAO());
    }

    /**
     * Test of getNewLineModelGridMetadata method, of class Recorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetNewLineModelGridMetadata() throws Exception {
//        System.out.println("getNewLineModelGridMetadata");
//        Arrays.asList(new ILeafTreeNode[] { m.siteThemeDatatype });
//        when(m.projetDAO.getAll(Projet.class)).thenReturn(
//                Arrays.asList(new Projet[] { m.projet }));
//        when(m.siteDAO.getAll(Site.class)).thenReturn(Arrays.asList(new Site[] { m.site }));
//        when(m.themeDAO.getAll(Theme.class)).thenReturn(
//                Arrays.asList(new Theme[] { m.theme }));
//        when(m.datatypeDAO.getAll(DataType.class)).thenReturn(
//                Arrays.asList(new DataType[] { m.datatype }));
//        when(m.datatype.getName()).thenReturn(MockUtils.DATATYPE);
//        when(m.theme.getName()).thenReturn(MockUtils.THEME);
//        instance.initModelGridMetadata();
//        LineModelGridMetadata result = instance.getNewLineModelGridMetadata(m.siteThemeDatatype);
//        Assert.assertTrue(4 == result.getColumnsModelGridMetadatas().size());
//        Assert.assertEquals(MockUtils.PROJET, result.getColumnModelGridMetadataAt(0).getValue());
//        Assert.assertEquals(MockUtils.SITE_PATH, result.getColumnModelGridMetadataAt(1).getValue());
//        Assert.assertEquals(MockUtils.THEME, result.getColumnModelGridMetadataAt(2).getValue());
//        Assert.assertEquals(MockUtils.DATATYPE, result.getColumnModelGridMetadataAt(3).getValue());
    }

    /**
     * Test of getProjetDAO method, of class Recorder.
     */
    @Test
    public void testGetProjetDAO() {
        instance = new Recorder();
        instance.setProjetDAO(m.projetDAO);
        Assert.assertEquals(m.projetDAO, instance.getProjetDAO());
    }

    /**
     * Test of getProjetSiteThemeDatatypeDAO method, of class Recorder.
     */
    @Test
    public void testGetProjetSiteThemeDatatypeDAO() {
        instance = new Recorder();
        instance.setProjetSiteThemeDatatypeDAO(m.projetSiteThemeDatatypeDAO);
        Assert.assertEquals(m.projetSiteThemeDatatypeDAO, instance.getProjetSiteThemeDatatypeDAO());
    }

    /**
     * Test of getSiteDAO method, of class Recorder.
     */
    @Test
    public void testGetSiteDAO() {
        instance = new Recorder();
        instance.setSiteDAO(m.siteDAO);
        Assert.assertEquals(m.siteDAO, instance.getSiteDAO());
    }

    /**
     * Test of getThemeDAO method, of class Recorder.
     */
    @Test
    public void testGetThemeDAO() {
        instance = new Recorder();
        instance.setThemeDAO(m.themeDAO);
        Assert.assertEquals(m.themeDAO, instance.getThemeDAO());
    }

    /**
     * Test of initModelGridMetadata method, of class Recorder.
     *
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    @Test
    public void testInitModelGridMetadata() throws PersistenceException {
        MockUtils.localizationManager = spy(MockUtils.localizationManager);
        instance.setLocalizationManager(MockUtils.localizationManager);
        instance.initModelGridMetadata();
        verify(m.projetDAO).getAll(org.inra.ecoinfo.symphonie.refdata.projet.Projet.class);
        verify(m.siteDAO).getAll(Site.class);
        verify(m.themeDAO).getAll(Theme.class);
        verify(m.datatypeDAO).getAll(DataType.class);
    }

    /**
     * Test of processRecord method, of class Recorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testProcessRecord() throws Exception {
        instance = spy(instance);
        mock(Projet.class);
        File file = new File("/home/ptcherniati/Soere/symphonie/symphonie-core-services-impl/src/test/resources/org/inra/ecoinfo/symphonie/metadata/projetsitethemedatatype/test1.csv");
        when(m.themeDAO.getByCode(MockUtils.THEME)).thenReturn(Optional.of(m.theme));
        String text = "nom du projet;nom du site;nom du thème;nom du type de données\n"
                + "projet;site;theme;datatype ";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        nodeables.add(m.projet);
        nodeables.add(m.site);
        nodeables.add(m.theme);
        nodeables.add(m.datatype);
        // nominal case nothing
        List<INode> root = new LinkedList();
        doReturn(Stream.of(m.variable)).when(m.recorder).getNodeables(VariableSymphonie.class);
        final Class[] entryInstances = new Class[]{Projet.class, SiteSymphonie.class, Theme.class, DataType.class};
        when(configuration.getEntryType()).thenReturn(entryInstances);
        doReturn(VariableSymphonie.class).when(configuration).getStickyLeafType();
        final Integer[] entryOrder = new Integer[]{1, 2, 3, 4};
        when(configuration.getEntryOrder()).thenReturn(entryOrder);
        ArgumentCaptor<Stream> orderedPathes = ArgumentCaptor.forClass(Stream.class);
        ArgumentCaptor< HashMap> entities = ArgumentCaptor.forClass(HashMap.class);
        ArgumentCaptor< Class[]> ti = ArgumentCaptor.forClass(Class[].class);
        ArgumentCaptor< List> ns = ArgumentCaptor.forClass(List.class);
        ICompositeTreeNode datatypeNode = mock(ICompositeTreeNode.class);
        when(datatypeNode.getNode()).thenReturn(m.projetSiteThemeDatatypeNode);
        ICompositeTreeNode themeNode = mock(ICompositeTreeNode.class);
        when(themeNode.getNode()).thenReturn(m.projetSiteThemeNode);
        ICompositeTreeNode siteNode = mock(ICompositeTreeNode.class);
        when(siteNode.getNode()).thenReturn(m.projetSiteNode);
        ICompositeTreeNode projetNode = mock(ICompositeTreeNode.class);
        when(projetNode.getNode()).thenReturn(m.projetNode);
        List<ICompositeTreeNode> returnListChild = new LinkedList();
        returnListChild.add(datatypeNode);
        returnListChild.add(themeNode);
        returnListChild.add(siteNode);
        returnListChild.add(projetNode);
        Map<String,TypeSite> map = mock(Map.class);
        root.add(m.projetSiteThemeDatatypeNode);
        when(m.MgaBuilder.buildLeavesForPathes(
                orderedPathes.capture(),
                ti.capture(), entities.capture(),
                eq(WhichTree.TREEDATASET),
                ns.capture()))
                .thenReturn(root.stream());
        doReturn(new LinkedList()).when(instance).getAllElements();
        when(m.projetSiteThemeDatatypeDAO.getTypesSiteMapBySiteName()).thenReturn(map);
        instance.processRecord(parser, file, encoding);
        verify(m.recorder, times(1)).persist(m.projetSiteThemeDatatypeNode);
        verify(applicationCacheManager).removeSkeletonTree(MgaIOConfigurator.DATASET_CONFIGURATION);
        verify(applicationCacheManager).removeSkeletonTree(MgaIOConfigurator.DATASET_CONFIGURATION_RIGHTS);
        verify(m.policyManager).clearTreeFromSession();
        when(map.get("oir/p1")).thenReturn(m.typeSite);
//        List<String> collect = (List<String>) orderedPathes.getValue().collect(Collectors.toList());
//        Assert.assertEquals("donnees_biologiques,typeSite,oir/p1,piegeage_en_montee,projet_manche", collect.get(0));
        Assert.assertArrayEquals(entryInstances, ti.getValue());
        Assert.assertEquals(m.datatype, entities.getValue().get("DataType/datatype"));
        Assert.assertEquals(m.theme, entities.getValue().get("Theme/theme"));
        Assert.assertEquals(m.site, entities.getValue().get("SiteSymphonie/site"));
        Assert.assertEquals(m.projet, entities.getValue().get("Projet/projet"));
        Assert.assertEquals(m.variable, ns.getValue().get(0));

    }

    /**
     * Test of processRecord method, of class Recorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    @Ignore
    public void testProcessRecordBadDatatype() throws Exception {
        BusinessException error = null;
        mock(Projet.class);
        when(m.themeDAO.getByCode(MockUtils.THEME)).thenReturn(Optional.of(m.theme));
        when(m.datatypeDAO.getByCode(MockUtils.DATATYPE)).thenReturn(null);
        String text = "nom du projet;nom du site;nom du thème;nom du type de données\n"
                + "projet;site;theme;datatype ";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        File file = null;
        // no site datatype
        ArgumentCaptor<INode> pstdn = ArgumentCaptor.forClass(INode.class);
        ArgumentCaptor<INode> pstn = ArgumentCaptor.forClass(INode.class);
        ArgumentCaptor<INode> psn = ArgumentCaptor.forClass(INode.class);
        ArgumentCaptor<INode> sn = ArgumentCaptor.forClass(INode.class);
        try {
            instance.processRecord(parser, file, encoding);
            fail("no errot");
        } catch (BusinessException e) {
            error = e;
        }
        Assert.assertEquals(
                ".Ligne 1 colonne 4, le type de données portant le code \"datatype\" n'existe pas en base de données\n",
                error.getMessage());
        verify(m.projetSiteThemeDatatypeDAO, never()).saveOrUpdate(pstdn.capture());
        verify(m.projetSiteThemeDatatypeDAO, never()).saveOrUpdate(pstn.capture());
        verify(m.projetSiteThemeDatatypeDAO, never()).saveOrUpdate(psn.capture());
        verify(m.projetSiteThemeDatatypeDAO, never()).saveOrUpdate(sn.capture());

    }

    /**
     * Test of processRecord method, of class Recorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    @Ignore
    public void testProcessRecordBadProjet() throws Exception {
        BusinessException error = null;
        mock(Projet.class);
        when(m.themeDAO.getByCode(MockUtils.THEME)).thenReturn(Optional.of(m.theme));
        when(m.projetDAO.getByCode(MockUtils.PROJET)).thenReturn(null);
        String text = "nom du projet;nom du site;nom du thème;nom du type de données\n"
                + "projet;site;theme;datatype ";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        File file = null;
        // no site projet
        ArgumentCaptor<INode> pstdn = ArgumentCaptor.forClass(INode.class);
        ArgumentCaptor<INode> pstn = ArgumentCaptor.forClass(INode.class);
        ArgumentCaptor<INode> psn = ArgumentCaptor.forClass(INode.class);
        ArgumentCaptor<INode> sn = ArgumentCaptor.forClass(INode.class);
        try {
            instance.processRecord(parser, file, encoding);
            fail("no errot");
        } catch (BusinessException e) {
            error = e;
        }
        Assert.assertEquals(
                ".Ligne 1 colonne 1, le projet portant le code \"projet\" n'existe pas en base de données\n",
                error.getMessage());
        verify(m.projetSiteThemeDatatypeDAO, never()).saveOrUpdate(pstdn.capture());
        verify(m.projetSiteThemeDatatypeDAO, never()).saveOrUpdate(pstn.capture());
        verify(m.projetSiteThemeDatatypeDAO, never()).saveOrUpdate(psn.capture());
        verify(m.projetSiteThemeDatatypeDAO, never()).saveOrUpdate(sn.capture());

    }

    /**
     * Test of processRecord method, of class Recorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    @Ignore
    public void testProcessRecordBadSite() throws Exception {
        BusinessException error = null;
        mock(Projet.class);
        when(m.themeDAO.getByCode(MockUtils.THEME)).thenReturn(Optional.of(m.theme));
        when(m.siteDAO.getByCodeAndParent(MockUtils.SITE_CODE, null)).thenReturn(null);
        String text = "nom du projet;nom du site;nom du thème;nom du type de données\n"
                + "projet;site;theme;datatype ";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        File file = null;
        // no site nothing
        ArgumentCaptor<INode> pstdn = ArgumentCaptor.forClass(INode.class);
        ArgumentCaptor<INode> pstn = ArgumentCaptor.forClass(INode.class);
        ArgumentCaptor<INode> psn = ArgumentCaptor.forClass(INode.class);
        ArgumentCaptor<INode> sn = ArgumentCaptor.forClass(INode.class);
        try {
            instance.processRecord(parser, file, encoding);
            fail("no errot");
        } catch (BusinessException e) {
            error = e;
        }
        Assert.assertEquals(
                ".Ligne 1 colonne 2, le site portant le code \"site\" n'existe pas en base de données\n",
                error.getMessage());
        verify(m.projetSiteThemeDatatypeDAO, never()).saveOrUpdate(pstdn.capture());
        verify(m.projetSiteThemeDatatypeDAO, never()).saveOrUpdate(pstn.capture());
        verify(m.projetSiteThemeDatatypeDAO, times(1)).saveOrUpdate(psn.capture());
        verify(m.projetSiteThemeDatatypeDAO, times(1)).saveOrUpdate(sn.capture());

    }

    /**
     * Test of processRecord method, of class Recorder.
     *
     * @throws java.lang.Exception
     */
    @Test
    @Ignore
    public void testProcessRecordBadTheme() throws Exception {
        BusinessException error = null;
        mock(Projet.class);
        when(m.themeDAO.getByCode(MockUtils.THEME)).thenReturn(null);
        String text = "nom du projet;nom du site;nom du thème;nom du type de données\n"
                + "projet;site;theme;datatype ";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        File file = null;
        // no theme nothing
        ArgumentCaptor<INode> pstdn = ArgumentCaptor
                .forClass(INode.class);
        ArgumentCaptor<INode> pstn = ArgumentCaptor.forClass(INode.class);
        ArgumentCaptor<INode> psn = ArgumentCaptor.forClass(INode.class);
        ArgumentCaptor<INode> sn = ArgumentCaptor.forClass(INode.class);
        try {
            instance.processRecord(parser, file, encoding);
            fail("no errot");
        } catch (BusinessException e) {
            error = e;
        }
        Assert.assertEquals(
                ".Ligne 1 colonne 3, le thème portant le code \"theme\" n'existe pas en base de données\n",
                error.getMessage());
        verify(m.projetSiteThemeDatatypeDAO, never()).saveOrUpdate(pstdn.capture());
        verify(m.projetSiteThemeDatatypeDAO, never()).saveOrUpdate(pstn.capture());
        verify(m.projetSiteThemeDatatypeDAO, times(1)).saveOrUpdate(psn.capture());
        verify(m.projetSiteThemeDatatypeDAO, times(1)).saveOrUpdate(sn.capture());

    }

}
