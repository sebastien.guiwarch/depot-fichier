/*
 * To change this license header, choose License Headers in Project Properties. To change this template file, choose Tools | Templates and open the template in the editor.
 */
package org.inra.ecoinfo.symphonie.refdata.typesite;

import com.Ostermiller.util.CSVParser;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Properties;
import org.inra.ecoinfo.localization.impl.SpringLocalizationManager;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.symphonie.refdata.typesite.TypeSite;
import org.inra.ecoinfo.symphonie.test.utils.MockUtils;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author ptcherniati
 */
public class RecorderTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    MockUtils  m        = MockUtils.getInstance();
    Recorder   instance;
    String     encoding = "UTF-8";

    @Mock
    Properties propertiesNomEN;

    @Mock
    Properties propertiesNomFR;

    @Mock
    Properties propertiesDefinitionEN;

    @Mock
    Properties propertiesDefinitionFR;

    /**
     *
     */
    public RecorderTest() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        m = MockUtils.getInstance();
        instance = new Recorder();
        MockitoAnnotations.initMocks(this);
        instance.setTypeSiteDAO(m.typeSiteDAO);
        ((SpringLocalizationManager) MockUtils.localizationManager)
                .setLocalizationDAO(m.localizationDAO);
        instance.setLocalizationManager(MockUtils.localizationManager);
        when(m.localizationDAO.getByNKey(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
                .thenReturn(Optional.empty());
        Mockito.when(
                MockUtils.localizationManager.newProperties(Nodeable.getLocalisationEntite(TypeSite.class), Nodeable.ENTITE_COLUMN_NAME,
                        Locale.FRANCE)).thenReturn(propertiesNomFR);
        Mockito.when(
                MockUtils.localizationManager.newProperties(Nodeable.getLocalisationEntite(TypeSite.class), Nodeable.ENTITE_COLUMN_NAME,
                        Locale.ENGLISH)).thenReturn(propertiesNomEN);
        Mockito.when(
                MockUtils.localizationManager.newProperties(Nodeable.getLocalisationEntite(TypeSite.class), "definition",
                        Locale.ENGLISH)).thenReturn(propertiesDefinitionEN);
        Mockito.when(m.typeSite.getCode()).thenReturn(MockUtils.TYPE_SITE);
        Mockito.when(m.typeSite.getDefinition()).thenReturn("définition");
        Mockito.when(propertiesNomFR.getProperty(MockUtils.TYPE_SITE)).thenReturn("typesitefr");
        Mockito.when(propertiesNomEN.getProperty(MockUtils.TYPE_SITE)).thenReturn("typesiteen");
        Mockito.when(propertiesDefinitionEN.getProperty("définition")).thenReturn("définitionen");
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of deleteRecord method, of class Recorder.
     * @throws java.lang.Exception
     */
    @Test
    public void testDeleteRecord() throws Exception {
        String text = "typesite;typesite;Plateforme de mesure;Measurement platform";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        File file = null;
        // nominal case
        instance.deleteRecord(parser, file, encoding);
        Mockito.verify(m.typeSiteDAO).getTypeSiteByCode(MockUtils.TYPE_SITE);
        Mockito.verify(m.typeSiteDAO).remove(m.typeSite);
        // bad type site code
        text = "badtypesite;typesite;Plateforme de mesure;Measurement platform";
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        BusinessException error = null;
        Mockito.when(m.typeSiteDAO.getTypeSiteByCode(Matchers.anyString())).thenReturn(Optional.empty());
        try {
            instance.deleteRecord(parser, file, encoding);
        } catch (BusinessException e) {
            error = e;
        }
        Assert.assertNotNull("une exception doit être lancée", error);
        Assert.assertEquals("can't get type site", error.getMessage());
        // persistence exception
        error = null;
        text = "typesite;typesite;Plateforme de mesure;Measurement platform";
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        Mockito.doThrow(new PersistenceException("error2")).when(m.typeSiteDAO).remove(m.typeSite);
        try {
            instance.deleteRecord(parser, file, encoding);
        } catch (BusinessException e) {
            error = e;
        }
        Assert.assertNotNull("une exception doit être lancée", error);
        Assert.assertEquals("can't get type site", error.getMessage());
    }

    /**
     * Test of getAllElements method, of class Recorder.
     * @throws java.lang.Exception
     */
    @Test
    public void testGetAllElements() throws Exception {
        final List<TypeSite> typeSites = Arrays.asList(new TypeSite[] { m.typeSite });
        Mockito.when(m.typeSiteDAO.getAll()).thenReturn(typeSites);
        List<TypeSite> result = instance.getAllElements();
        Mockito.verify(m.typeSiteDAO).getAll();
        Assert.assertTrue(1 == result.size());
        Assert.assertEquals(typeSites.get(0), result.get(0));
    }

    /**
     * Test of getNewLineModelGridMetadata method, of class Recorder.
     * @throws java.lang.Exception
     */
    @Test
    public void testGetNewLineModelGridMetadata() throws Exception {
        instance.initModelGridMetadata();
        LineModelGridMetadata result = instance.getNewLineModelGridMetadata(m.typeSite);
        Assert.assertTrue(5 == result.getColumnsModelGridMetadatas().size());
        Assert.assertEquals("typesite", result.getColumnModelGridMetadataAt(0).getValue());
        Assert.assertEquals("typesitefr", result.getColumnModelGridMetadataAt(1).getValue());
        Assert.assertEquals("typesiteen", result.getColumnModelGridMetadataAt(2).getValue());
        Assert.assertEquals("définition", result.getColumnModelGridMetadataAt(3).getValue());
        Assert.assertEquals("définitionen", result.getColumnModelGridMetadataAt(4).getValue());
        Mockito.verify(m.typeSite, Mockito.times(1)).getCode();
        Mockito.verify(m.typeSite, Mockito.times(1)).getDefinition();
        Mockito.verify(propertiesNomFR).getProperty(MockUtils.TYPE_SITE);
        Mockito.verify(propertiesNomEN).getProperty(MockUtils.TYPE_SITE);
        Mockito.verify(propertiesDefinitionEN).getProperty("définition");
    }

    /**
     * Test of initModelGridMetadata method, of class Recorder.
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    @Test
    public void testInitModelGridMetadata() throws PersistenceException {
        MockUtils.localizationManager = Mockito.spy(MockUtils.localizationManager);
        instance.setLocalizationManager(MockUtils.localizationManager);
        instance.initModelGridMetadata();
        Mockito.verify(MockUtils.localizationManager).newProperties(Nodeable.getLocalisationEntite(TypeSite.class), Nodeable.ENTITE_COLUMN_NAME, Locale.ENGLISH);
        Mockito.verify(MockUtils.localizationManager).newProperties(Nodeable.getLocalisationEntite(TypeSite.class), "definition", Locale.ENGLISH);
    }

    /**
     * Test of processRecord method, of class Recorder.
     * @throws java.lang.Exception
     */
    @Test
    public void testProcessRecord() throws Exception {
        String text = "tze_nom_fr;tze_nom_en;tze_definition_fr;tze_definition_en\n"
                + "typesite;typesite;typesite;Plateforme de mesure;Measurement platform";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        File file = null;
        // nominal case
        Mockito.when(m.typeSiteDAO.getTypeSiteByCode(MockUtils.TYPE_SITE)).thenReturn(Optional.empty());
        ArgumentCaptor<TypeSite> ts = ArgumentCaptor.forClass(TypeSite.class);
        instance.processRecord(parser, file, encoding);
        Mockito.verify(m.typeSiteDAO).saveOrUpdate(ts.capture());
        Assert.assertEquals(MockUtils.TYPE_SITE, ts.getValue().getCode());
        Assert.assertEquals("Plateforme de mesure", ts.getValue().getDefinition());
        // with existing db
        Mockito.when(m.typeSiteDAO.getTypeSiteByCode(MockUtils.TYPE_SITE)).thenReturn(Optional.of(m.typeSite));
        ts = ArgumentCaptor.forClass(TypeSite.class);
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        instance.processRecord(parser, file, encoding);
        Mockito.verify(m.typeSiteDAO, Mockito.times(2)).saveOrUpdate(ts.capture());
        Assert.assertEquals(m.typeSite, ts.getValue());
        Mockito.verify(m.typeSite).setDefinition("Plateforme de mesure");
        // with null site
        text = "tze_nom_fr;tze_nom_en;tze_definition_fr;tze_definition_en\n"
                + ";typesite;typesite;Plateforme de mesure;Measurement platform";
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        Mockito.when(m.typeSiteDAO.getTypeSiteByCode(MockUtils.TYPE_SITE)).thenReturn(Optional.of(m.typeSite));
        try {
            instance.processRecord(parser, file, encoding);
            Assert.fail("error not thrown");
        } catch (BusinessException e) {
            Assert.assertEquals(".Ligne 1, colonne 1, le type de site n'est pas renseigné.\n",
                    e.getMessage());
        }

    }

    /**
     * Test of setTypeSiteDAO method, of class Recorder.
     */
    @Test
    public void testSetTypeSiteDAO() {
        instance = Mockito.spy(new Recorder());
        instance.setTypeSiteDAO(m.typeSiteDAO);
        Mockito.verify(instance).setTypeSiteDAO(m.typeSiteDAO);
    }

}
